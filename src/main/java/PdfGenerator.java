import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class PdfGenerator {
    private static String FILE = "resume.pdf";
    public PdfGenerator(){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            document.newPage();
            addTitle(document);
            addTable(document);
            document.close();
        } catch (Exception e) {
        }
    }

    private static void addTitle(Document document) throws DocumentException {
            Paragraph preface = new Paragraph();
            Paragraph title = new Paragraph("Resume", FontFactory.getFont(FontFactory.COURIER, 32));
            title.setAlignment(Element.ALIGN_CENTER);
            preface.add(title);
            preface.setSpacingAfter(50);
            document.add(preface);

    }
    private static void addTable(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(2);

        table.addCell("First name");
        table.addCell("Mateusz");
        table.addCell("Last name");
        table.addCell("Madel");
        table.addCell("Profession");
        table.addCell("Student");
        table.addCell("Education");
        table.addCell("PWSZ Tarnow 2018-now");
        table.addCell("Summary");
        table.addCell("...");

        document.add(table);
    }

}
